
angular.module('appDemo.services', [])

.factory('BackendService', ['$http', function ($http) {

  var svc = {};

  svc.getFeeds = function(){
    return $http.get('sampledata/tickets.json');
  }

  svc.getProducts = function(){
    return $http.get('sampledata/forms.json');
  }

  return svc;
}])

.factory('Tickets', function() {

  var tickets = [
    {
        id : 0,
        code : '128039-2',
        date : '01/01/2017',
        model : 'Ford Explorer',
        address : 'Hamlet 4145, las condes',
        client : 'Harry Yemer', 
        phone : '+56946721234',
        image : 'sampledata/images/1.png',
        description : 'Revisión Técnica',
        state : 'new'
    },
    {
        id : 1,
        code : '794562-2',
        date : '04/01/2017',
        model : 'Audi A3',
        address : 'Providencia 6145',
        client : 'Samuel Garcia', 
        phone : '+56978594321',
        image : 'sampledata/images/2.png',
        description : 'Revisión Técnica',
        state : 'finished'
    },
    {
        id : 2,
        code : '687212-4',
        date : '29/12/2016',
        model : 'Toyota Camry',
        address : 'Los Corteses 5832',
        client : 'Francisco Vergara', 
        phone : '+56948456780',
        image : 'sampledata/images/3.png',
        description : 'Mantención',
        state : 'new'
    },
    {
        id : 3,
        code : '128642-2',
        date : '09/01/2017',
        model : 'Renault Megane',
        address : 'Vergara 262',
        client : 'Moises Junco', 
        phone : '+56979343431',
        image : 'sampledata/images/8.png',
        description : 'Conductor de Reemplazo',
        state : 'new'
    },
       {
        id : 4,
        code : '433467-2',
        date : '31/12/2016',
        model : 'Mazda3',
        address : 'lord cochrane 417',
        client : 'Moises Junco', 
        phone : '+56979343431',
        image : 'sampledata/images/7.png',
        description : 'Mantención',
        state : 'finished'
    },
    {
        id : 5,
        code : '31332-2',
        date : '05/01/2017',
        model : 'Citröen C4',
        address : 'Plaza Egaña 30, Ñuñoa',
        client : 'Santiago Segura', 
        phone : '+5697789421',
        image : 'sampledata/images/4.png',
        description : 'Revisión Técnica',
        state : 'open'
    },
     {
        id : 6,
        code : '89082-8',
        date : '02/01/2017',
        model : 'Honda CR-V',
        address : 'Las condes 9050',
        client : 'Lenin Varguillas', 
        phone : '+569434321',
        image : 'sampledata/images/5.png',
        description : 'Revisión Técnica',
        state : 'open'
    },
     {
        id : 7,
        code : '576888-2',
        date : '03/01/2017',
        model : 'KIA Sportage',
        address : 'apoquindo 7071',
        client : 'Maria Ignacia Torres', 
        phone : '+569765221',
        image : 'sampledata/images/6.png',
        description : 'Revisión Técnica',
        state : 'finished'
    }
  ];

  return {
    all: function() {
      return tickets;
    },
    remove: function(ticket) {
      tickets.splice(tickets.indexOf(ticket), 1);
    },
    get: function(ticketId) {
      for (var i = 0; i < tickets.length; i++) {
        if (tickets[i].id === parseInt(ticketId)) {
          return tickets[i];
        }
      }
      return null;
    }
  };
})

.factory('Forms', function() {

  var forms = [
    {
        id : 0,
        code : '128039-2',
        date : '01/01/2017',
        type : 'Revisión Técnica'
    },
    {
        id : 1,
        code : '794562-2',
        date : '04/01/2017',
        type : 'Mantención'
    },
    {
        id : 2,
        code : '687212-4',
        date : '29/12/2016',
        type : 'Revisión Técnica'
    },
    {
        id : 3,
        code : '128642-2',
        date : '09/01/2017',
        type : 'Revisión Técnica'
    },
       {
        id : 4,
        code : '433467-2',
        date : '31/12/2016',
        type : 'Mantención'
    },
    {
        id : 5,
        code : '31332-2',
        date : '05/01/2017',
        type : 'Conductor de Reemplazo'
    },
     {
        id : 6,
        code : '89082-8',
        date : '02/01/2017',
        type : 'Conductor de Reemplazo'
    },
     {
        id : 7,
        code : '576888-2',
        date : '03/01/2017',
        type : 'Mantención'
    }
  ];

  return {
    all: function() {
      return forms;
    },
    remove: function(form) {
      forms.splice(forms.indexOf(form), 1);
    },
    get: function(formId) {
      for (var i = 0; i < forms.length; i++) {
        if (forms[i].id === parseInt(formId)) {
          return forms[i];
        }
      }
      return null;
    }
  };
});