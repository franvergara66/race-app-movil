
angular.module('appDemo', ['ionic', 'appDemo.controllers', 'appDemo.services', 'ngCordova'])




.run(function($ionicPlatform, $rootScope, $timeout, $state) {
  $ionicPlatform.ready(function() {
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);
    }
    if (window.StatusBar) {
      StatusBar.styleDefault();
    }

  
    $rootScope.$on('$stateChangeStart', 
      function(event, toState, toParams, fromState, fromParams){
        if(toState.data && toState.data.auth == true && !$rootScope.user.email){
          event.preventDefault();
          $state.go('app.login');   
        }
    });

  });
})

.config(function($stateProvider, $urlRouterProvider) {

  $stateProvider

  .state('app', {
    url: '/app',
    abstract: true,
    templateUrl: 'templates/menu.html',
    controller: 'AppCtrl'
  })
  
  .state('app.start', {
    url: '/start',
    views: {
      'menuContent': {
        templateUrl: 'templates/start.html'
      }
    }
  })

  .state('app.login', {
    url: '/login',
    cached : false,
    views: {
      'menuContent': {
        templateUrl: 'templates/login.html',
        controller : 'LoginCtrl'
      }
    }
  })

  .state('app.forgot', {
    url: '/forgot',
    views: {
      'menuContent': {
        templateUrl: 'templates/forgot.html'
      }
    }
  })

  .state('app.main', {
    url: '/main',
    data : { auth : true },
    cache : false,
    views: {
      'menuContent': {
        templateUrl: 'templates/main.html',
        controller : 'MainCtrl'
      }
    }
  })

  .state('app.account', {
      url: '/account',
      data : { auth : true },
      views: {
        'menuContent': {
          templateUrl: 'templates/account.html',
          controller : 'AccountCtrl'
        }
      }
  })

  
  .state('app.messages', {
    url: '/messages',
    data : { auth : true },
    cache : false,
    views: {
      'menuContent': {
        templateUrl: 'templates/messages.html',
        controller : 'MessagesCtrl'
      }
    }
  })
   .state('app.jobs_new', {
      url: '/jobs_new',
      data : { auth : true },
      views: {
        'menuContent': {
          templateUrl: 'templates/jobs_new.html',
          controller: 'TicketsCtrl'
        }
      }
    })

  .state('app.ticket_new', {
      url: '/jobs_new/:ticketId',
      data : { auth : true },
      views: {
        'menuContent': {
          templateUrl: 'templates/ticket_new.html',
          controller: 'TicketDetailCtrl'
        }
      }
    })

  .state('app.jobs_open', {
      url: '/jobs_open',
      data : { auth : true },
      views: {
        'menuContent': {
          templateUrl: 'templates/jobs_open.html',
          controller: 'TicketsCtrl'
        }
      }
    })

  .state('app.ticket_open', {
      url: '/jobs_open/:ticketId',
      data : { auth : true },
      views: {
        'menuContent': {
          templateUrl: 'templates/ticket_open.html',
          controller: 'TicketDetailCtrl'
        }
      }
    })

   .state('app.jobs_finished', {
      url: '/jobs_finished',
      data : { auth : true },
      views: {
        'menuContent': {
          templateUrl: 'templates/jobs_finished.html',
          controller: 'TicketsCtrl'
        }
      }
    })

    .state('app.ticket_finished', {
      url: '/jobs_finished/:ticketId',
      data : { auth : true },
      views: {
        'menuContent': {
          templateUrl: 'templates/ticket_finished.html',
          controller: 'TicketDetailCtrl'
        }
      }
    })

  .state('app.forms_list', {
      url: '/forms_list',
      data : { auth : true },
      views: {
        'menuContent': {
          templateUrl: 'templates/forms_list.html',
          controller: 'FormsCtrl'
        }
      }
    })

  .state('app.forms', {
      url: '/forms/:formId',
      data : { auth : true },
      views: {
        'menuContent': {
          templateUrl: 'templates/forms.html',
          controller: 'FormDetailCtrl'
        }
      }
    })

 // si fallan de va a inicio
  $urlRouterProvider.otherwise('/app/start');

});
