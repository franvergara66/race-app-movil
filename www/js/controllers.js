
angular.module('appDemo.controllers', [])

//top view controller
.controller('AppCtrl', function($scope, $rootScope, $state) {

  $rootScope.user = {};

  $scope.logout = function(){
    $rootScope.user = {};
    $state.go('app.start')
  };

})


.controller('LoginCtrl', function ($scope, $state, $rootScope) {

    $scope.login = function(){
    //Aqui obtenemos los datos de la BD y los mostramos en main
    $rootScope.user = {
      email : "francisco@race.cl",
      name : "Francisco Vergara",
      address : "Providencia 6145",
      city : "Santiago",
      phone  : "+56948436331",
      position : "Técnico en Terreno", 
      avatar : 'sampledata/images/avatar.jpg'
    };
    //Una vez tomados los datos redirigimos al main 
    $state.go('app.main');
  };
})

//top view controller
.controller('AppCtrl', function($scope, $rootScope, $state) {
  $rootScope.user = {};
  $scope.logout = function(){
    $rootScope.user = {};
    $state.go('app.start')
  };
})


.controller('AccountCtrl', function($scope, $rootScope) {  

  $scope.readonly = true;
  $scope.accountUser = angular.copy($rootScope.user);
  var userCopy = {};

  $scope.startEdit = function(){
    $scope.readonly = false;
    userCopy = angular.copy($scope.user);
  };

  $scope.cancelEdit = function(){
    $scope.readonly = true;
    $scope.user = userCopy;
  };

  $scope.saveEdit = function(){
    $scope.readonly = true;
    $rootScope.user = $scope.accountUser;
  };

})


.controller('JobsCtrl', function($scope, BackendService) {
  
  $scope.doRefresh = function(){
      BackendService.getFeeds()
      .success(function(newItems) {
        $scope.feeds = newItems;
      })
      .finally(function() {
        // Stop the ion-refresher from spinning
        $scope.$broadcast('scroll.refreshComplete');
      });
  };

  $scope.doRefresh();

})

.controller('MainCtrl', function($scope, $ionicActionSheet, BackendService) {
  
  $scope.doRefresh = function(){
      BackendService.getProducts()
      .success(function(newItems) {
        $scope.products = newItems;
      })
      .finally(function() {
        // Stop the ion-refresher from spinning (not needed in this view)
        $scope.$broadcast('scroll.refreshComplete');
      });
  };

  $scope.getDate = function(){
      $scope.date = new Date();
  }
  

  $scope.doRefresh();

})


.controller('MessagesCtrl', function($scope, $ionicListDelegate) {
  
})

.controller('TicketsCtrl', function($scope, Tickets) {

  $scope.tickets = Tickets.all();
  $scope.remove = function(ticket) {
    Tickets.remove(ticket);
  };
})

.controller('TicketDetailCtrl', function($scope, $stateParams, Tickets) {
  $scope.ticket = Tickets.get($stateParams.ticketId);
})

.controller('FormsCtrl', function($scope, Forms) {

  $scope.forms = Forms.all();
  $scope.remove = function(form) {
    Forms.remove(form);
  };
})

.controller('FormDetailCtrl', function($scope, $stateParams, Forms) {
  $scope.form = Forms.get($stateParams.formId);
})



